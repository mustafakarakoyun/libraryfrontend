import React from 'react';
import ReactDOM from 'react-dom';
import Card from './components/card';

//const names = ["mustafa", "mert","burak"]
//<button type="button" style={{padding:'10px',backgroundcolor:"11111"}}> { Bunun içinede dışarıran bir şey koyarsak }</button> Style etiketini kullanma örneği
function App(){ //return ın içinde herşey tek bir div içerisinde olmalı imiş
    return (
        <div>
            <div className="card-group">
                <Card cardTitle="Galatasaray"/>
                <Card cardTitle="Fener" />
                <Card cardTitle="Beşiktaş"/>
            </div>
        </div>
    )
}

ReactDOM.render(
    <App/>,
    document.getElementById('root')
);
